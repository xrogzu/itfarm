<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<%--图片滚动--%>
<div id="slider">

    <div class="container">

        <ul>
            <%--每一项--%>
            <c:forEach items="${animations }" var="item" varStatus="vs">
                <li>
                        <%--图片--%>
                    <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="1010" height="330" alt="" />
                        <%--内容--%>
                    <div class="slide-info">

                        <h2><a href="${ctx }/article/detail.do?id=${item.article.recordId}">${item.article.title }</a></h2>

                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.lastDate}</a></p>

                        <p><itfarm:StringCut length="200" strValue="${item.article.content }"></itfarm:StringCut></p>

                        <a href="#" class="comments-count">44 评论</a>

                        <a href="${ctx }/article/detail.do?id=${item.article.recordId}" class="slider-button">查看更多...</a>

                    </div><!-- end .slide-info -->

                </li>
            </c:forEach>

        </ul>

    </div><!-- end .container -->

</div><!-- end #slider -->
</body>
</html>