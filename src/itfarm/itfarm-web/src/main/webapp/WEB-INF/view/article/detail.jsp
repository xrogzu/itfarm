<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title> ${config.webTitle} </title>
  <meta charset="utf-8">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="nav-shadow"></div>
<div id="content">

  <div class="container">

    <div id="main">

      <div class="entry single">

        <div class="entry-header">

          <h2 class="title">${item.article.title }&nbsp;<a href="${ctx }/article/detail.do?id=${item.article.recordId}&widescreen=true" style="font-size: 10px;color: #8BBF5D;">(宽屏浏览)</a></h2>

          <p class="meta">Published on <a href="#">${item.lastDate }</a> by <a href="#">${item.authorName }</a></p>
          <div class="post-rating">
            <a href="#"><span class="on"></span></a>
            <a href="#"><span class="on"></span></a>
            <a href="#"><span class="on"></span></a>
            <a href="#"><span class="off"></span></a>
            <a href="#"><span class="off"></span></a>
            <p>评价星级</p>
          </div>

        </div><!-- end .entry-header -->

        <div class="entry-content">

          <div class="zoom">
            <a class="single_image" href="${config.adminUrl }titleImg/${item.article.titleImg}">
              <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="650" height="210" alt="Texas Trip 2010: Abandoned ranch" class="entry-image" />
            </a>
          </div>
          <p>${item.article.content }</p>

        </div><!-- end .entry-content -->

        <div class="box-footer">
          <div class="align-left">
            <c:if test="${item.last != null }">
              <ul>
                <li><strong>上一篇:</strong>&nbsp;</li>
                <li><a href="${ctx }/article/detail.do?id=${item.last.recordId}&widescreen=false">${item.last.title }</a></li>
              </ul>
            </c:if>
          </div><!-- end .align-left -->
          <div class="align-right">
            <c:if test="${item.next != null }">
              <ul>
                <li><strong>下一篇:</strong>&nbsp;</li>
                <li><a href="${ctx }/article/detail.do?id=${item.next.recordId}&widescreen=false">${item.next.title }</a></li>
                </li>
              </ul>
            </c:if>
          </div><!-- end .align-right -->
        </div><!-- end .box-footer -->

        <div class="box-footer">
          <%--分类--%>
          <div class="align-left">

            <ul>
              <li><strong>Category:</strong>&nbsp;</li>
              <li><a href="#">${item.categoryName}</a> <span class="separator">,</span>&nbsp;</li>
              <li><a href="#">其它</a></li>
            </ul>

          </div><!-- end .align-left -->
          <%--标签--%>
          <div class="align-right">

            <ul>
              <li><strong>Tags:</strong>&nbsp;</li>
              <li><a href="#">${item.article.keyword }</a> <span class="separator">,</span>&nbsp;</li>
              </li>
            </ul>

          </div><!-- end .align-right -->

        </div><!-- end .box-footer -->

        <div class="box-footer">

          <div class="align-left">

            <strong>Share this via:</strong>

          </div><!-- end .align-left -->
          <%--分享文章--%>
          <div class="align-right">

            <ul class="social-links">
              <li><a href="#"><img src="img/icons/social-icons/digg.png" alt="Digg" /></a></li>
              <li><a href="#"><img src="img/icons/social-icons/facebook.png" alt="Facebook" /></a></li>
              <li><a href="#"><img src="img/icons/social-icons/flickr.png" alt="Flickr" /></a></li>
              <li><a href="#"><img src="img/icons/social-icons/twitter.png" alt="Twittew" /></a></li>
              <li><a href="#"><img src="img/icons/social-icons/delicious.png" alt="Delicious" /></a></li>
              <li><a href="#"><img src="img/icons/social-icons/tumblr.png" alt="Tumblr" /></a></li>
            </ul>

          </div><!-- end .align-right -->

        </div><!-- end .box-footer -->

      </div><!-- end .entry -->

      <div class="author-bio">
        <img src="http://1.gravatar.com/avatar/5d4da935e9e04a8e72fa7b216350bced?s=100" width="100" height="100" alt="Gravatar" class="avatar" />
        <h4>About Smuliii</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into...</p>
        <a href="#" class="button">Read More...</a>
      </div>

      <div id="comments">

        <div class="box-header">

          <div class="align-left">
            <span class="comments-count"><em>16</em></span> <h6>评论</h6>&emsp;<a href="#">Trackbacks/Pingbacks</a>
          </div>

          <div class="align-right">
            <strong><a href="#respond">+立即评论</a></strong>
          </div>

        </div>

        <ol class="comments-list">

          <li class="comment first">

            <div class="comment-avatar">

              <img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" width="60" height="60" alt="Gravatar" class="avatar" />

            </div><!-- .comment-avatar -->

            <div class="comment-body">

              <div class="comment-meta">

                <a href="#">John Doe</a>, <span class="date">March 14. 2010</span>

              </div><!-- .comment-meta -->

              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

              <ul class="comment-links">
                <li><a href="#">Send to a Friend</a> <span class="separator">|</span></li>
                <li><a href="#">Share this Comment</a> <span class="separator">|</span></li>
                <li><a href="#">Reply</a></li>
              </ul><!-- .comment-links -->

            </div><!-- .comment-body -->

          </li><!-- .comment -->

          <li class="comment">

            <div class="comment-avatar">

              <img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" width="60" height="60" alt="Gravatar" class="avatar" />

            </div><!-- .comment-avatar -->

            <div class="comment-body">

              <div class="comment-meta">

                <a href="#">John Doe</a>, <span class="date">March 14. 2010</span>

              </div><!-- .comment-meta -->

              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

              <ul class="comment-links">
                <li><a href="#">Send to a Friend</a> <span class="separator">|</span></li>
                <li><a href="#">Share this Comment</a> <span class="separator">|</span></li>
                <li><a href="#">Reply</a></li>
              </ul><!-- .comment-links -->

            </div><!-- .comment-body -->

          </li><!-- .comment -->

          <li class="comment">

            <div class="comment-avatar">

              <img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" width="60" height="60" alt="Gravatar" class="avatar" />

            </div><!-- .comment-avatar -->

            <div class="comment-body">

              <div class="comment-meta">

                <a href="#">John Doe</a>, <span class="date">March 14. 2010</span>

              </div><!-- .comment-meta -->

              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

              <ul class="comment-links">
                <li><a href="#">Send to a Friend</a> <span class="separator">|</span></li>
                <li><a href="#">Share this Comment</a> <span class="separator">|</span></li>
                <li><a href="#">Reply</a></li>
              </ul><!-- .comment-links -->

            </div><!-- .comment-body -->

          </li><!-- .comment -->

        </ol>

      </div><!-- #comments -->

      <ul class="pagination">
        <li class="prev"><a href="#">Prev</a></li>
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li class="next"><a href="#">Next</a></li>
      </ul>

      <div id="respond" class="box">

        <div class="box-header">

          <h6 class="align-left">Leave a <span>Comment</span></h6>

          <p class="align-right">
            <strong><a href="#">Login to send reply</a></strong>
          </p>

        </div>

        <form method="post" action="" id="comment-form" class="form clearfix">

          <div class="input_block">

            <p>
              <label for="name">Name <span>(Required)</span></label>
              <input type="text" name="name" id="name" class="input" value="" placeholder="Your full name here">
            </p>

            <p>
              <label for="email">Email <span>(Required)</span></label>
              <input type="text" name="email" id="email" class="input" value="" placeholder="Email address here">
            </p>

            <p>
              <label for="website">Website <span>(Not Required)</span></label>
              <input type="text" name="website" id="website" class="input" value="" placeholder="Website URL here">
            </p>

          </div>

          <div class="textarea_block">

            <p>
              <label for="comment">Comments <span>(Use valid HTML tags)</span></label>
              <textarea id="comment" rows="10" cols="45" class="input"></textarea>
            </p>

            <p>
              <input type="submit" name="submit" value="Submit" class="submit" />
            </p>

          </div>

        </form>

      </div><!-- end #respond -->

    </div><!-- end #main -->
    <%--右侧竖栏--%>
    <jsp:include page="../index/right_menu.jsp"/>
    <div class="clear"></div>

  </div><!-- end .container -->


</div><!-- end #content -->
<jsp:include page="../index/right.jsp"></jsp:include>
<%--底部--%>
<div id="footer">

  <div class="container clearfix">

    <a href="index.html"><img src="img/footer-logo.png" alt="footer-logo" class="footer-logo" /></a>

    <div class="one-third">

      <h4>About Our Community</h4>

      <p>Lorem Ipsum is simply dummy text of the <a href="#">printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic...</p>

      <p>Lorem Ipsum is simply dummy <a href="#">text of the printing</a> and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an...</p>

      <strong><a href="#">Find more...</a></strong>

    </div><!-- end .one-third -->

    <div class="one-fourth">

      <h4>Categories</h4>

      <ul id="categories">
        <li><a href="#">Photography</a></li>
        <li><a href="#">Private Portfolios</a></li>
        <li><a href="#">My Travels</a></li>
        <li><a href="#">Family Travel Guide</a></li>
        <li><a href="#">Fly&amp;Drive Guides</a></li>
        <li><a href="#">Tropical Destinations</a></li>
        <li><a href="#">Mountains Desctinations</a></li>
      </ul>

      <strong class="align-center"><a href="#">Find more...</a></strong>

    </div><!-- end .one-fourth -->

    <div class="two-fifth last">

      <h4><span>Latest</span> Tweets</h4>

      <ul id="latest-tweets">

        <li>
          <a href="#" class="user">@medioworks</a>
          <p class="tweet">Lorem Ipsum is simply dummy text of <a href="#">the printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text...</p>
        </li>

        <li>
          <a href="#" class="user">@medioworks</a>
          <p class="tweet">Lorem Ipsum is simply <a href="#">dummy text</a> of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
        </li>

        <li>
          <a href="#" class="user">@medioworks</a>
          <p class="tweet">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
        </li>

      </ul><!-- end #latest-tweets -->

      <strong class="align-right"><a href="#">Join Conversation</a></strong>

    </div><!-- end .one-misc -->

  </div><!-- end .container -->

</div><!-- end #footer -->

<div id="footer-bottom">

  <div class="container">

    <p class="align-left">ITFARM - Copyright 2010, All Rights Reserved.</p>

    <ul class="align-right">
      <li><a href="#">Login</a><span class="separator">|</span></li>
      <li><a href="#">SignUp</a><span class="separator">|</span></li>
      <li><a href="#">Support</a><span class="separator">|</span></li>
      <li><a href="#">Contact Us</a></li>
    </ul>

  </div><!-- end .container -->

</div><!-- end #footer-bottom -->

</body>
</html>