<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title> ${config.webTitle} </title>
  <meta charset="utf-8">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="nav-shadow"></div>
<div id="content">

  <div class="container">

    <div id="main" class="fullwidth">

      <div class="entry single">

        <div class="entry-header">

          <h2 class="title">${item.article.title }&nbsp;<a href="${ctx }/article/detail.do?id=${item.article.recordId}&widescreen=false" style="font-size: 10px;color: #8BBF5D;">(返回窄屏)</a></h2>

          <p class="meta">Published on <a href="#">${item.lastDate }</a> by <a href="#">${item.authorName }</a></p>

        </div><!-- end .entry-header -->

        <div class="entry-content">
          <%--广告图片--%>
          <div class="zoom align-right">
            <a class="single_image" href="img/sample-images/800x600.jpg">
              <img src="img/sample-images/650x210.jpg" width="210" height="210" alt="Texas Trip 2010: Abandoned ranch" class="entry-image" />
            </a>
          </div>

          <div class="zoom align-left">
            <a class="single_image" href="img/sample-images/800x600.jpg">
              <img src="img/sample-images/650x210.jpg" width="210" height="210" alt="Texas Trip 2010: Abandoned ranch" class="entry-image" />
            </a>
          </div>

          <p>${item.article.content }</p>

        </div><!-- end .entry-content -->

        <div class="entry-footer">

          <strong class="align-left">操 作: &nbsp;</strong>

          <dl class="horizontal">
            <dt>上一篇:</dt>
            <dd><a href="${ctx }/article/detail.do?id=${item.last.recordId}&widescreen=true">${item.last.title }</a> <span class="separator">|</span></dd>
            <dt>下一篇:</dt>
            <dd><a href="${ctx }/article/detail.do?id=${item.next.recordId}&widescreen=true">${item.next.title }</a> <span class="separator">|</span></dd>
            <dt><a href="${ctx }/article/detail.do?id=${item.article.recordId}&widescreen=false">评论</a></dt>
          </dl>

        </div><!-- end .entry-footer -->

      </div><!-- end .entry -->

    </div><!-- end #main -->

  </div><!-- end .container -->

</div><!-- end #content -->
<jsp:include page="../index/right.jsp"></jsp:include>
<%--底部--%>
<div id="footer">

  <div class="container clearfix">

    <a href="index.html"><img src="img/footer-logo.png" alt="footer-logo" class="footer-logo" /></a>

    <div class="one-third">

      <h4>About Our Community</h4>

      <p>Lorem Ipsum is simply dummy text of the <a href="#">printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic...</p>

      <p>Lorem Ipsum is simply dummy <a href="#">text of the printing</a> and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an...</p>

      <strong><a href="#">Find more...</a></strong>

    </div><!-- end .one-third -->

    <div class="one-fourth">

      <h4>Categories</h4>

      <ul id="categories">
        <li><a href="#">Photography</a></li>
        <li><a href="#">Private Portfolios</a></li>
        <li><a href="#">My Travels</a></li>
        <li><a href="#">Family Travel Guide</a></li>
        <li><a href="#">Fly&amp;Drive Guides</a></li>
        <li><a href="#">Tropical Destinations</a></li>
        <li><a href="#">Mountains Desctinations</a></li>
      </ul>

      <strong class="align-center"><a href="#">Find more...</a></strong>

    </div><!-- end .one-fourth -->

    <div class="two-fifth last">

      <h4><span>Latest</span> Tweets</h4>

      <ul id="latest-tweets">

        <li>
          <a href="#" class="user">@medioworks</a>
          <p class="tweet">Lorem Ipsum is simply dummy text of <a href="#">the printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text...</p>
        </li>

        <li>
          <a href="#" class="user">@medioworks</a>
          <p class="tweet">Lorem Ipsum is simply <a href="#">dummy text</a> of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
        </li>

        <li>
          <a href="#" class="user">@medioworks</a>
          <p class="tweet">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
        </li>

      </ul><!-- end #latest-tweets -->

      <strong class="align-right"><a href="#">Join Conversation</a></strong>

    </div><!-- end .one-misc -->

  </div><!-- end .container -->

</div><!-- end #footer -->

<div id="footer-bottom">

  <div class="container">

    <p class="align-left">ITFARM - Copyright 2010, All Rights Reserved.</p>

    <ul class="align-right">
      <li><a href="#">Login</a><span class="separator">|</span></li>
      <li><a href="#">SignUp</a><span class="separator">|</span></li>
      <li><a href="#">Support</a><span class="separator">|</span></li>
      <li><a href="#">Contact Us</a></li>
    </ul>

  </div><!-- end .container -->

</div><!-- end #footer-bottom -->

</body>
</html>