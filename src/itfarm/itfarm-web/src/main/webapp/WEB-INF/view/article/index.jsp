<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> ${config.webTitle} </title>
    <meta charset="utf-8">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="nav-shadow"></div>
<jsp:include page="../index/slider.jsp"/>
<div id="content">

    <div class="container">

        <div id="main">

            <div id="trips-viewer" class="box">

                <div class="box-header">

                    <ul class="tabs-nav">
                        <li><a href="#featured-trips">推荐阅读</a></li>
                        <li><a href="#popular-trips">最新发布</a></li>
                        <li><a href="#most-viewed-trips" >最新评论</a></li>
                    </ul>

                    <a href="#" class="rss"><img src="${ctx}/images/icon-rss-small.png" alt="icon-rss" /></a>

                </div><!-- end .box-header -->
                <%--推荐阅读--%>
                <div id="featured-trips" class="tab-content">

                    <div class="trips-container">
                        <%--右边四个--%>
                        <ul>
                            <c:forEach items="${newArticles }" var="item">
                                <li id="featured-slide-one" class="trip-content">

                                    <a href="${ctx }/article/detail.do?id=${item.article.recordId}">
                                        <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="242" height="88" alt="" />
                                        <h4 class="title">${item.article.title}</h4>
                                    </a>
                                    <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>

                                    <p><itfarm:StringCut length="200" strValue="${item.article.content }"></itfarm:StringCut></p>

                                    <a href="#" class="sent-to-friend">Sent to a Friend</a>
                                    <a class="button" href="${ctx }/article/detail.do?id=${item.article.recordId}">Read More...</a>

                                </li><!-- end #slide-one -->
                            </c:forEach>
                            <%--<li id="featured-slide-two" class="trip-content">

                                <a href="single-post.html">
                                    <img src="${ctx}/new/img/sample-images/242x88.jpg" width="242" height="88" alt="" />
                                    <h4 class="title">Keep Off: Beach Attention Table</h4>
                                </a>
                                <p class="meta">Published on <a href="#">March 18, 2010</a> by <a href="#">John Doe</a></p>

                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an <a href="#">unknown printer took</a> a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic. </p>

                                <a href="#" class="sent-to-friend">Sent to a Friend</a>
                                <a class="button" href="single-post.html">Read More...</a>

                            </li><!-- end #slide-two -->

                            <li id="featured-slide-three" class="trip-content">

                                <a href="single-post.html">
                                    <img src="${ctx}/new/img/sample-images/242x88.jpg" width="242" height="88" alt="" />
                                    <h4 class="title">Museum: Native American Style Box</h4>
                                </a>
                                <p class="meta">Published on <a href="#">March 16, 2010</a> by <a href="#">John Doe</a></p>

                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an <a href="#">unknown printer took</a> a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>

                                <a href="#" class="sent-to-friend">Sent to a Friend</a>
                                <a class="button" href="single-post.html">Read More...</a>

                            </li><!-- end #slide-three -->

                            <li id="featured-slide-four" class="trip-content">

                                <a href="single-post.html">
                                    <img src="${ctx}/new/img/sample-images/242x88.jpg" width="242" height="88" alt="" />
                                    <h4 class="title">Abroad Trip: Australian Rocks</h4>
                                </a>
                                <p class="meta">Published on <a href="#">March 08, 2010</a> by <a href="#">John Doe</a></p>

                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an <a href="#">unknown printer took</a> a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>

                                <a href="#" class="sent-to-friend">Sent to a Friend</a>
                                <a class="button" href="single-post.html">Read More...</a>

                            </li><!-- end #slide-three -->--%>

                        </ul>

                    </div><!-- end .trips-container -->

                    <ul class="trips-nav">
                        <c:forEach items="${newArticles }" var="item" varStatus="vs">
                            <c:if test="${vs.count <= 4}">
                                <li class="nav-button">
                                    <div class="content">
                                        <a href="#featured-slide-one" class="title">
                                            <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="60" height="60" alt="" />
                                            <h6>${item.article.title}</h6>
                                        </a>
                                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>
                                        <ul class="links">
                                            <li><a href="#">122 Comments</a> <span class="separator">|</span></li>
                                            <li><a href="#">9 Tweets</a> <span class="separator">|</span></li>
                                            <li><a href="${ctx }/article/detail.do?id=${item.article.recordId}">Read More...</a></li>
                                        </ul>
                                    </div><!-- end .content -->
                                </li><!-- end .nav-button -->
                            </c:if>
                        </c:forEach>
                    </ul><!-- end .trips-nav -->

                </div><!-- end #featured-trips -->
                <%--最新发布--%>
                <div id="popular-trips" class="tab-content">

                    <div class="trips-container">

                        <ul>

                            <c:forEach items="${animations }" var="item">
                                <li id="popular-slide-one" class="trip-content">

                                    <a href="${ctx }/article/detail.do?id=${item.article.recordId}">
                                        <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="242" height="88" alt="" />
                                        <h4 class="title">${item.article.title}</h4>
                                    </a>
                                    <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>

                                    <p><itfarm:StringCut length="200" strValue="${item.article.content }"></itfarm:StringCut></p>

                                    <a href="#" class="sent-to-friend">Sent to a Friend</a>
                                    <a class="button" href="${ctx }/article/detail.do?id=${item.article.recordId}">Read More...</a>

                                </li><!-- end #slide-one -->
                            </c:forEach>
                        </ul>

                    </div><!-- end .trips-container -->

                    <ul class="trips-nav">

                        <c:forEach items="${animations }" var="item" varStatus="vs">
                            <c:if test="${vs.count <= 4}">
                                <li class="nav-button">
                                    <div class="content">
                                        <a href="#featured-slide-one" class="title">
                                            <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="60" height="60" alt="" />
                                            <h6>${item.article.title}</h6>
                                        </a>
                                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>
                                        <ul class="links">
                                            <li><a href="#">122 Comments</a> <span class="separator">|</span></li>
                                            <li><a href="#">9 Tweets</a> <span class="separator">|</span></li>
                                            <li><a href="${ctx }/article/detail.do?id=${item.article.recordId}">Read More...</a></li>
                                        </ul>
                                    </div><!-- end .content -->
                                </li><!-- end .nav-button -->
                            </c:if>
                        </c:forEach>
                    </ul><!-- end .trips-nav -->

                </div><!-- end #popular-trips -->
                <%--最新评论--%>
                <div id="most-viewed-trips" class="tab-content">

                    <div class="trips-container">

                        <ul>

                            <c:forEach items="${animations }" var="item">
                                <li id="most-slide-one" class="trip-content">

                                    <a href="${ctx }/article/detail.do?id=${item.article.recordId}">
                                        <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="242" height="88" alt="" />
                                        <h4 class="title">${item.article.title}</h4>
                                    </a>
                                    <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>

                                    <p><itfarm:StringCut length="200" strValue="${item.article.content }"></itfarm:StringCut></p>

                                    <a href="#" class="sent-to-friend">Sent to a Friend</a>
                                    <a class="button" href="${ctx }/article/detail.do?id=${item.article.recordId}">Read More...</a>

                                </li><!-- end #slide-one -->
                            </c:forEach>

                        </ul>

                    </div><!-- end .trips-container -->

                    <ul class="trips-nav">
                        <c:forEach items="${animations }" var="item" varStatus="vs">
                            <c:if test="${vs.count <= 4}">
                                <li class="nav-button">
                                    <div class="content">
                                        <a href="#featured-slide-one" class="title">
                                            <img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="60" height="60" alt="" />
                                            <h6>${item.article.title}</h6>
                                        </a>
                                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>
                                        <ul class="links">
                                            <li><a href="#">122 Comments</a> <span class="separator">|</span></li>
                                            <li><a href="#">9 Tweets</a> <span class="separator">|</span></li>
                                            <li><a href="${ctx }/article/detail.do?id=${item.article.recordId}">Read More...</a></li>
                                        </ul>
                                    </div><!-- end .content -->
                                </li><!-- end .nav-button -->
                            </c:if>
                        </c:forEach>
                    </ul><!-- end .trips-nav -->

                </div><!-- end #most-viewed-trips -->

            </div><!-- end #trips-viewer -->
            <%--文章显示每个div--%>
            <c:forEach items="${articles }" var="item" varStatus="vs">
                <div class="entry">

                    <div class="entry-header">

                        <h2 class="title"><a href="${ctx }/article/detail.do?id=${item.article.recordId}">${item.article.title}</a></h2>

                        <p class="meta">编辑于 <a href="#">${item.lastDate}</a> by <a href="#">${item.authorName}</a></p>

                        <a href="${ctx }/article/detail.do?id=${item.article.recordId}" class="button">Read More...</a>

                    </div><!-- end .entry-header -->

                    <div class="entry-content">

                        <a href="${ctx }/article/detail.do?id=${item.article.recordId}"><img src="${config.adminUrl }titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/001.png'" width="240" height="140" alt="" class="entry-image" /></a>

                        <p><itfarm:StringCut length="350" strValue="${item.article.content }"></itfarm:StringCut></p>

                        <hr />

                        <ul class="entry-links">
                            <li><a href="#">Send to a Friend</a> <span class="separator">|</span></li>
                            <li><a href="#">Share this Post</a> <span class="separator">|</span></li>
                            <li><a href="#">41 Comments</a> <span class="separator">|</span></li>
                            <li><a href="#">0 Tweets</a></li>
                        </ul>

                    </div><!-- end .entry-content -->

                </div><!-- end .entry -->
            </c:forEach>
            <%--分页--%>
            <itfarm:PageBar pageUrl="/article/index.do" pageAttrKey="page"></itfarm:PageBar>

        </div><!-- end #main -->
        <%--右侧竖栏--%>
        <jsp:include page="../index/right_menu.jsp"/>
        <div class="clear"></div>

    </div><!-- end .container -->


</div><!-- end #content -->
<jsp:include page="../index/right.jsp"></jsp:include>
<%--底部--%>
<div id="footer">

    <div class="container clearfix">

        <a href="index.html"><img src="img/footer-logo.png" alt="footer-logo" class="footer-logo" /></a>

        <div class="one-third">

            <h4>About Our Community</h4>

            <p>Lorem Ipsum is simply dummy text of the <a href="#">printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic...</p>

            <p>Lorem Ipsum is simply dummy <a href="#">text of the printing</a> and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an...</p>

            <strong><a href="#">Find more...</a></strong>

        </div><!-- end .one-third -->

        <div class="one-fourth">

            <h4>Categories</h4>

            <ul id="categories">
                <li><a href="#">Photography</a></li>
                <li><a href="#">Private Portfolios</a></li>
                <li><a href="#">My Travels</a></li>
                <li><a href="#">Family Travel Guide</a></li>
                <li><a href="#">Fly&amp;Drive Guides</a></li>
                <li><a href="#">Tropical Destinations</a></li>
                <li><a href="#">Mountains Desctinations</a></li>
            </ul>

            <strong class="align-center"><a href="#">Find more...</a></strong>

        </div><!-- end .one-fourth -->

        <div class="two-fifth last">

            <h4><span>Latest</span> Tweets</h4>

            <ul id="latest-tweets">

                <li>
                    <a href="#" class="user">@medioworks</a>
                    <p class="tweet">Lorem Ipsum is simply dummy text of <a href="#">the printing and typesetting</a> industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                </li>

                <li>
                    <a href="#" class="user">@medioworks</a>
                    <p class="tweet">Lorem Ipsum is simply <a href="#">dummy text</a> of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                </li>

                <li>
                    <a href="#" class="user">@medioworks</a>
                    <p class="tweet">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                </li>

            </ul><!-- end #latest-tweets -->

            <strong class="align-right"><a href="#">Join Conversation</a></strong>

        </div><!-- end .one-misc -->

    </div><!-- end .container -->

</div><!-- end #footer -->

<div id="footer-bottom">

    <div class="container">

        <p class="align-left">ITFARM - Copyright 2010, All Rights Reserved.</p>

        <ul class="align-right">
            <li><a href="#">Login</a><span class="separator">|</span></li>
            <li><a href="#">SignUp</a><span class="separator">|</span></li>
            <li><a href="#">Support</a><span class="separator">|</span></li>
            <li><a href="#">Contact Us</a></li>
        </ul>

    </div><!-- end .container -->

</div><!-- end #footer-bottom -->

</body>
</html>