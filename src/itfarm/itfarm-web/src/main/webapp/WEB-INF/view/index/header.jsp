<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title> ${config.webTitle} </title>
</head>
<body>
<%--顶部导航--%>
<div id="header-top">

  <div class="container">

    <a href="#"><img src="${ctx}/images/icon-rss-large.png" alt="icon-rss-large" class="rss-icon" /></a>

    <p class="left">www.asjava.cn |
      <c:if test="${user == null}">
        <a href="${ctx}/login/toLogin.do">登陆</a> | <a href="${ctx}/login/toLogin.do?type=reg">注册</a>
      </c:if>
      <c:if test="${user != null}">
        <a href="#">${user.nickname}</a> | <a href="${ctx}/login/loginOut.do">注销</a>
      </c:if>
    </p>

    <p class="right">Subscribe to <a href="#">RSS</a> | <a href="#">Email</a> | <strong>122 Subscribers</strong></p>

  </div><!-- end .container -->

</div><!-- end #header-top -->
<%--logo部分--%>
<div id="header">

  <div class="container">
    <%--logo图片--%>
    <h1 id="logo">
      <a href="index.html">
        <img src="${ctx}/images/logo.jpg" alt="Travel Guide">
      </a>
    </h1>
    <%--右侧用于广告--%>
    <div id="header-ads">

      <a href="#?ref=smuliii"><img src="http://envato.s3.amazonaws.com/referrer_adverts/tf_468x60_v4.gif" alt="tf_468x60_v4" /></a>

    </div><!-- end #header-ads -->

  </div><!-- end .container -->

</div><!-- end #header -->
<%--菜单导航栏--%>
<div id="nav">

  <div class="container">

    <ul>
      <li <c:if test="${menuSelected == 0}">class="active"</c:if>><a href="${ctx}/">主页</a></li>
      <c:forEach items="${menus}" var="menu" varStatus="vs">
        <c:if test="${menu.parent == null}">
        <li <c:if test="${menuSelected == menu.recordId}">class="active"</c:if>><a href="${ctx}${menu.url}?menuId=${menu.recordId}">${menu.name}</a>
        <%--<ul>
          <li><a href="#">JAVA</a></li>
          <li><a href="#">PHP</a></li>
          <li><a href="#">.NET</a></li>
          <li><a href="#">C++</a></li>
          <li><a href="#">WEB</a></li>
        </ul>--%>
        </li>
        </c:if>
      </c:forEach>
      <li><a href="#">更多</a>
        <ul>
          <li><a href="${config.adminUrl}/admin/index.do">后台管理</a></li>
        </ul>
      </li>
    </ul>
    <%--搜索栏--%>
    <div id="search">

      <input type="text" style="height: auto;" class="input" value="" placeholder="Keyword search here...">
      <input type="submit" class="submit" value="Go">

    </div><!-- end #search -->

  </div><!-- end .container -->

</div><!-- end #nav -->
</body>
</html>