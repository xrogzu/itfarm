package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.ArticleCriteria;
import com.tc.itfarm.model.ext.ArticleEx;

import java.util.Map;

public interface ArticleDao extends SingleTableDao<Article, ArticleCriteria> {
    ArticleEx selectLast(String title);

    ArticleEx selectNext(String title);

    void changeCategory(Map<String, Object> map);
}