package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Role implements Serializable {
    private Integer recordId;

    private String name;

    private String remark;

    private Date createTime;

    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    public Role(Integer recordId, String name, String remark, Date createTime, Date modifyTime) {
        this.recordId = recordId;
        this.name = name;
        this.remark = remark;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

    public Role() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}