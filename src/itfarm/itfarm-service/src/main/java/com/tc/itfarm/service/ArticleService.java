package com.tc.itfarm.service;

import java.util.List;

import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.ext.ArticleEx;

public interface ArticleService extends BaseService<Article> {
	PageList<Article> selectArticleByPage(Page page, Integer categoryId, String title);

	void save(Article article, Integer menuId);

	/**
	 * 根据类型查询
	 * @param e
	 * @param count 每页数量
	 * @return
	 */
	List<Article> selectArticles(QueryArticleEnum e, int count);

	/**
	 * 查询上一篇
	 * @param title
	 * @return
	 */
	ArticleEx selectLast(String title);

	/**
	 * 查询下一篇
	 * @param title
	 * @return
	 */
	ArticleEx selectNext(String title);

	/**
	 * 文章移动到某分类
	 * @param categoryId
	 * @param ids
	 */
	void modifyToCategory(Integer categoryId, List<Integer> ids);

	/**
	 * 删除文章
	 * @param ids
	 */
	void delete(List<Integer> ids);

	/**
	 * 根据id查询
	 * @param ids
	 * @return
	 */
	List<Article> selectByIds(List<Integer> ids);
}
